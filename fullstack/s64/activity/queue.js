let collection = [];

// Write the queue functions  below.

// PRINT
function print() {
    return collection;
}

// ENQUEUE
function enqueue(element) {
    collection[collection.length] = element;
    return collection;
}

// DEQUEUE
function dequeue() {

    for (let i = 0; i < collection.length - 1; i++) {
      collection[i] = collection[i + 1];
    }

    collection.length--;
    return collection;
}

// FRONT
function front() {
    return collection[0];
}

// SIZE
function size() {
    return collection.length;
}

// isEmpty
function isEmpty() {
    if(collection.length == 0){
        return true;
    } else {
        return false;
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};