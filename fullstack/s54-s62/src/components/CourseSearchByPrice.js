import React, { useState } from 'react';

const CourseSearch = () => {
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [courses, setCourses] = useState([]);
  const [loading, setLoading] = useState(false);

  const handleSearch = async () => {
    setLoading(true);

    const searchParams = {
      minPrice: parseFloat(minPrice),
      maxPrice: parseFloat(maxPrice),
    };

    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/courses/searchByPrice`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(searchParams),
      });

      const data = await response.json();
      // Filter out inactive courses here
      const activeCourses = data.courses.filter(course => course.isActive);
      setCourses(activeCourses);
    } catch (error) {
      console.error('Error fetching courses:', error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className="container mt-5">
      <div className="row">
        <div className="form-group">
          <h2>Course Search by Price</h2>
          <div className="mb-3">
            <label htmlFor="minPrice" className="form-label">Min Price</label>
            <input
              type="number"
              id="minPrice"
              className="form-control"
              value={minPrice}
              onChange={(e) => setMinPrice(e.target.value)}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="maxPrice" className="form-label">Max Price</label>
            <input
              type="number"
              id="maxPrice"
              className="form-control"
              value={maxPrice}
              onChange={(e) => setMaxPrice(e.target.value)}
            />
          </div>
          <button
            className="btn btn-primary"
            onClick={handleSearch}
            disabled={loading}
          >
            {loading ? 'Searching...' : 'Search'}
          </button>
        </div>
      </div>
      <div className="row mt-4">
        <div className="col-md-8 offset-md-2">
          <h3>Search Results by Price</h3>
          <ul className="list-group">
            {courses.map((course) => (
              <li key={course.id} className="list-group-item">{course.name} - Php{course.price}</li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default CourseSearch;
