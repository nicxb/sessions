import { useState, useEffect } from 'react';
import CourseSearch from './CourseSearch';
import CourseSearchByPrice from './CourseSearchByPrice';
import CourseCard from './CourseCard';

import { Row, Col } from 'react-bootstrap';

export default function UserView({coursesData}) {

	const [courses, setCourses] = useState([]);

	useEffect(() => {
		const coursesArr = coursesData.map(course => {
			if(course.isActive === true) {
				return (
					<CourseCard courseProp={course} key={course._id} />
				)
			} else {
				return null;
			}
		})

		setCourses(coursesArr);
	}, [coursesData])


	return (

		<>
			<Row>
				<h1 className="text-center">Courses</h1>
				<Col className="border col-md-6 col-12">
					<CourseSearch />
				</Col>
				<Col  className="border p-5 col-md-6 col-12">
					<CourseSearchByPrice />
				</Col>
			</Row>
			
			
			
			{ courses }
		</>
	)

}