import React, { useState } from 'react';





const UpdateProfile = ({ fetchData }) => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [message, setMessage] = useState('');
  
  const handleUpdate = async () => {
    const token = localStorage.getItem('token');
    const url = `${process.env.REACT_APP_API_URL}/users/profile`;
    
    const data = {
      firstName: firstName,
      lastName: lastName,
      mobileNo: mobileNo,
    };
    
    const headers = {
      'Authorization': `Bearer ${token}`,
      'Content-Type': 'application/json',
    };
    
    try {
      const response = await fetch(url, {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify(data),
      });

      // const responseData = await response.json();

      if (response.ok) {
        setMessage('Profile updated successfully');
        setFirstName('');
        setLastName('');
        setMobileNo('');
        fetchData();
      } else {
        setMessage('Profile update failed');
      }
    } catch (error) {
      setMessage('An error occurred');
    }
  };
  
  return (
    <div className="container">
      <h2>Update Profile</h2>
      <div className="form-group">
        <label>First Name:</label>
        <input
          type="text"
          className="form-control"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        />
      </div>
      <div className="form-group">
        <label>Last Name:</label>
        <input
          type="text"
          className="form-control"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        />
      </div>
      <div className="form-group">
        <label>Mobile Number:</label>
        <input
          type="text"
          className="form-control"
          value={mobileNo}
          onChange={(e) => setMobileNo(e.target.value)}
        />
      </div>
      <button className="btn btn-primary mt-4" onClick={handleUpdate}>
        Update Profile
      </button>
      <p>{message}</p>
    </div>
  );
};

export default UpdateProfile;
