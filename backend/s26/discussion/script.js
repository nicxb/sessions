// [SECTION] Arrays and Indexes
let grades = [98.5, 94.3, 89.2, 90.1];
let computer_brands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu", "Lenovo"];
let mixed_array = [12, "Asus", null, undefined, {}];

// Alternative way to write arrays
let my_tasks = [
	"drink html",
	"eat javascript",
	"inhale CSS",
	"bake sass"
]

// console.log(my_tasks);

// Reassigning values
console.log("Array before reassignment");
console.log(my_tasks);

my_tasks[0] = "run hello world"; // To reassign a value in an array, just use its index number and use an assignment operator to replace the value of that index

console.log("Array after reassignment:");
console.log(my_tasks);


// [SECTION] Reading from Arrays
console.log(computer_brands[1]);
console.log(grades[3]);

// Getting the length of an Array
console.log(computer_brands.length);

// Accessing last element in an array
let index_of_last_element = computer_brands.length - 1;

console.log(computer_brands[index_of_last_element]);


// [SECTION] Array Methods
let fruits = ["Apple", "Orange", "Kiwi", "Passiongfruit"];

console.log("Current array: ");
console.log(fruits);

// fruits.push("Mango");
fruits.push("Mango", "Cocomelon");

console.log("Updated array after push method:");
console.log(fruits);


// Pop Method
console.log("Current array: ");
console.log(fruits);

let remove_item = fruits.pop();


console.log("Updated array after pop method:");
console.log(fruits);
console.log("Removed fruit: " + remove_item);


// Unshift Method
console.log("Current array: ");
console.log(fruits);

fruits.unshift("Lime", "Star Apple");

console.log("Updated array after unshift method:");
console.log(fruits);

// Shift Method
console.log("Current array: ");
console.log(fruits);

fruits.shift();

console.log("Updated array after shift method:");
console.log(fruits);


// Splice Method
console.log("Current array: ");
console.log(fruits);

// "1" index number
// "2" no. of items na tatanggalin
//  splice(startingIndex, numberOfItemsToBeDeleted, itemsToBeAdded)
fruits.splice(1, 2, "Lime", "Cherry");

console.log("Updated array after splice method:");
console.log(fruits);


// Sort Method 
console.log("Current array: ");
console.log(fruits);

fruits.sort();

console.log("Updated array after sort method:");
console.log(fruits);


// [Sub-section] Non-Mutator Methods - Every methods written above is called a mutator method because it modifies the value of he array one wa or another. Non'mutator methods on the tother hand, don't do the same thing. They instead execute specific functionalities that can be done with the existing array values.

// indexOf method
let index_of_lenovo = computer_brands.indexOf("Lenovo");

console.log("The index of Lenovo is: " + index_of_lenovo);

// lastIndexOf method - ghets the index of a specific item starting form the end of array. This will only work if there are multiple instances of the same item. The item closest to the end of the array will be the one that the method will get the index of.
let index_of_lenovo_from_last_item = computer_brands.lastIndexOf("Lenovo");
console.log("The index of lenovo starting from the end of the array is: " + index_of_lenovo_from_last_item);


// Slice Method
let hobbies = ["Gaming", "Running", "Gaslighting", "Cycling", "Writing"];


let slice_array_from_hobbies = hobbies.slice(2);
console.log(slice_array_from_hobbies);
console.log(hobbies);

// dead end ang index 3
let slice_array_from_hobbies_B = hobbies.slice(2, 3);
console.log(slice_array_from_hobbies_B);
console.log(hobbies);

let slice_array_from_hobbies_C = hobbies.slice(-3);
console.log(slice_array_from_hobbies_C);
console.log(hobbies);


// toString Method
let string_array = hobbies.toString();
console.log(string_array);

// conCat Method
let greeting = ["hello", "world"];
let exclamation = ["!", "?"];

let concat_greeting = greeting.concat(exclamation);
console.log(concat_greeting);

//join Method - This will convert the array to a string AND insert a specified separator between them. The separator is defined as the argument of the join() function.
// console.log(hobbies.join(', '));
console.log(hobbies.join(' - '));


// foreach Method
// it works similarly wit hthe for statement. It's just that it will only stop it's loop once all of the items inside the array has been passed(?).
// the "hobbies" is passed as a parameter in a function
// does not return anything
hobbies.forEach(function(hobby){
	console.log(hobby);
})
// In real-life scenarios, nagagamit ang forEach method sa mga prducts and services where-in you will need to call back each data in a database


// map Method - Loops throughout the whole array and adds each item to a new array.
// it returns a new array. It also works in a loop pattern but it will generate a new array
let numbers_list = [1, 2, 3, 4, 5];

let numbers_map = numbers_list.map(function(number){
	return number * 2;
})

console.log(numbers_map);


// filter Method
let filtered_numbers = numbers_list.filter(function(number){
	return (number < 3);
})

console.log(filtered_numbers);


// [SECTION] Multi-dimensional Arrays
let chess_board = [
	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
	['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
	['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
	['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
	['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
	['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
	['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
	['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'],
];

console.log(chess_board[1][4]);
