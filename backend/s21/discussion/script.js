// FUNCTION DECLARATION & INVOCATION
// function printName(){
// 	console.log("My name is Jeff");
// }

// printName();

// FUNCTION EXPRESSION
// let variable_function = function(){
// 	console.log("Hello from function expression!");
// }

// variable_function();

// SCOPING
let global_variable = "Call me mr. worldwide";

console.log(global_variable);

function showNames() {
	let function_variable = "Joe";
	const function_const = "John";

	console.log(function_variable);
	console.log(function_const);

	// you CAN use global variables inside any function as long as they are declared outside of the function scope.
	console.log(global_variable);
}

//You cannot use locally-scoped variables outside the function they are declared in. 
// console.log(function_variable);

showNames();

// NESTED FUNCITONS
function parentFunction(){
	let name = "Jane";

	function childFunction(){
		let nested_name = "John";

		// will work because it is considered as a global scope within sa parent function
		console.log(name);

		// Accessing the nested_name variable within the same functino it was declared in WILL work. 
		console.log(nested_name);
	}

	childFunction();

	// console.log(nested_name); //this will result in an error. Accessing the nested_name variable outside the function it was declared in WILL NOT work.
}

parentFunction();


// BEST PRACTICE FOR FUNCTION NAMING
function printWelcomeMessageForUser(){
	let first_name = prompt("Enter your first name: ");
	let last_name = prompt("Enter your last name: ");

	console.log("Hello, " + first_name + last_name + "!");
	console.log("Welcome sa page ko!");
}

// printWelcomeMessageForUser();


// RETURN STATEMENT
function fullName(){
	// return "Earl Rafael Diaz";
	console.log("Earl Rafael Diaz");
}

// console.log(fullName());
fullName();
