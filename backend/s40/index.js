// Server variables for initialization
const express = require('express'); // import express
// require directive

const app = express(); // Initializes express
const port = 4000;

// Middleware
app.use(express.json()); // REgistering a middleware that will make express de able to read JSON format from requests
// json.parse; will convert the json request into regular javascript

// si express ay natanggap lamang ng array or string format.
// kaya need ng extended true
app.use(express.urlencoded({extended: true})); // middleware that will allow express to be able to read data types other than the default string and array it can usually read.


// Server Listening
app.listen(port, () => console.log(`Server is running at port ${port}`));


// [SECTION] Routes
app.get('/', (request, response) => {
	response.send('Hello World!');
}) 

app.post('/greeting', (request, response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}`);
})


// Mock database
let users = [];

app.post('/register', (request, response) => {
	if(request.body.username !== '' && request.body.password !== '') {
		users.push(request.body);
		response.send(`User ${request.body.username} has successfully been registered!`);
	} else {
		response.send(`Please input BOTH username AND password.`);
	}
})

// gets the array of users
app.get('/users', (request, response) => {
	response.send(users);
})


module.exports = app;
