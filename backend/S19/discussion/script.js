// Javascript consists of whats called 'statements'. And statements are basically just syntax with semi-colons at the end.
// alert("Hello World!");

// Wala siyang semi-colon pero nagwowork??
// Javascript can access the 'log' function of the console to display text/data in teh console.
console.log("Hello World!");

// [SECTION] Variables

// Variable declaration: initialize yung varaiable (same as in the C language)
// let my_variable;
// "let": pang initialize
// Variable Declaration & Invocation
let my_variable = "Hola, Mundo!";
console.log(my_variable);

// Concatenating Strings : proseso ng pagkakabit ng dalawang variables together
let country = "Philippines";
let province = "Metro Manila";
let full_address = province + ", " + country;

console.log(full_address);

// Numbers/Integers
let headcount = 26;
let grade = 98.7;
console.log("The number of students is " + headcount + " and the average grade of all students is " + grade);

// let sum = headcount + grade;

// console.log(sum)

// Boolean - The value of boolean is only true of false. When naming variables that have a boolean value, make sure they're formatted like a question.
let isMarried = false;
let isGoodConduct = true;

console.log("He's married: " + isMarried);
console.log("She's a good person: " + isGoodConduct);

// Array -set of data
let grades = [98.7, 89.9, 90.2, 94.6];
let details = ["John", "Smith", 32, true];

console.log(details);

// Objects
let person = {
	fullName: "Juan Dela Cruz",
	age: 40,
	isMarried: false,
	contact: ["09992223313", "09448876691"],
	address: {
		houseNumber: "345",
		city: "England"
	}
}
 console.log(person);
// Javascript reads arrays as objects. This is mainly to accomodate for specific functionalities that arrays can do later on.
console.log(typeof person);
console.log(typeof grades);

// Null & Undefined
let girlfriend = null;
let full_name;

console.log(girlfriend);
console.log(full_name);

// [SECTION] Operators

// Arithmetic Operators
let first_number = 5;
let second_number = 5;

let sum = first_number + second_number;
let difference = first_number - second_number;
let product = first_number * second_number;
let quotient = first_number / second_number;
let remainder = first_number % second_number;

console.log(sum);
console.log(difference);
console.log(product);
console.log(quotient);
console.log(remainder);

// Assignement Operators
let assignment_number = 0;

assignment_number = assignment_number + 2;
console.log("Result of addtiion assignment operator: " + assignment_number);

assignment_number += 2;
console.log("Result o shorthand addition assignment operator: " + assignment_number);