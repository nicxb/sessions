// console.log("Array Mutator Methods");

// Mutator: changes the array
// Non-mutator: does not change the original array
// Iterator: Goes/loops through your array to perform specific tasks

// Iteration Method
/*
    - loops through all the elements to perform repetative tasks on the array
*/

// forEach() - to loop through the array
// map() - loops through the array and returns a new array
// filter - it returns a new array containitng elements which meets the given condition

// every() - it checks if all the elements meet the given condition
// return true if all elements meet the given condition, however, false is its does not
let numbers = [1, 2, 3, 4, 5, 6];

let allValid = numbers.every(function(number) {
    return numbers > 3;
})

console.log("Result of every() method: ");
console.log(allValid);

// some() - checks if atleast one elemetn meets the given condition 
// returns true if atleast one elemetn meets the given condition, return false other wise
let someValid = numbers.some(function(number) {
    return number < 2;
})

console.log('result of some() method: ');
console.log(someValid);

//  include()  method - methods can be "chained" using them one after another
let products = ['Mouse', 'keyboard', 'Laptop', 'Monitor'];

let filteredProducts = products.filter(function(product) {
    return product.toLowerCase().includes('a');
})

console.log(filteredProducts);

// reduce()
let iteration = 0;

let reducedArray = numbers.reduce(function(x, y) {

    console.warn('current iteration: ' + ++iteration);
    console.log('accumulator: ' + x);
    console.log('currentValue: ' + y);

    return x + y;
})

console.log('result of reduce method: ' + reducedArray);

let productsReduce = products.reduce(function(x, y) {

    return x + ' ' + y;
})

console.log('result of reduce() method: ' + productsReduce);