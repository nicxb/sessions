// [SECTION] While Loop
// let count = 5;

// while (count !== 0) {
// 	console.log("Current value of count: " + count);
// 	count--;
// }


// [SECTION] Do-while Loop
// Para maging number yung data typpe, gagawin natin siyang Number()
// /kasi magiging string lang yung input na number
// let number = Number(prompt("Give me a number:"));

// do {
// 	console.log("Current value of number: " + number);
// 	number += 1;
// } while (number < 10)


// [SECTION] For Loop
// for (let count = 0; count <= 20; count++) {
// 	console.log("Current For Loop Value: " + count);
// }



// let my_string = "earl";

// To get the length of a string
// console.log(my_string.length);

// To get a specific letter in a string
// console.log(my_string[2]);


// Loops through each letter in the string and will keep iterating as long as the current index is less than the length of the string.
// for (let index = 0; index < my_string.length; index++) {
// 	console.log(my_string[index]);
// }


// MINI ACTIVITY (20 mins)
// Loop through the 'my_name' variable which has a string with your name on it.
// Display each letterin the console but exclude all the vowels from it 
// let my_name = "Jennica Basnillo"

// console.log(my_name[2]);
// console.log(my_name[3]);

// for (let index = 0; index < my_string.length; index++) {
// 	console.log(my_name[index]);
// }

// for (let index = 0; index < my_name.length; index++) {
//   let letter = my_name[index].toLowerCase();
   
//   if (letter !== 'a' && letter !== 'e' && letter !== 'i' && letter !== 'o' && letter !== 'u') {
//     console.log(my_name[index]);
//   }
// }


// [SECTION] Break and Continue
let name_two = "rafael";

for(let index = 0; index < name_two.length; index++){
	// console.log(name_two[index]);

	if(name_two[index].toLowerCase() == "a") {
		console.log("Skipping...");
		continue;
	}

	if (name_two[index] == "e"){
		break;
	}
	
}


