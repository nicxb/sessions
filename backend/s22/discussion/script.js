// ARGUMENTS AND PARAMETERS
function printName(name){
	console.log("I'm the real " + name);
}

printName("Slim Shady");

function checkDivisibilityBy2(number){
	let result = number % 2;
	console.log("The remainder of " + number + " is " + result);
}

checkDivisibilityBy2(15);


// MULTIPLE AGUMENTS AND PARAMETERS

function createFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

createFullName("Juan", "Dela", "Cruz");
// createFullName("Juan", "Dela",); // will result for the third one to be undefined.

	// function createFullName(firstName, middleName){
	// 	console.log(firstName + " " + middleName);
	// }

	// createFullName("Juan", "Dela", "Cruz"); // Juan Dela nalnag yung lumabas


// USAGE OF PROMPTS AND ALERTS
// let user_name = prompt("Enter your username:");

function displayWelcomeMessageForUser(userName){
	alert("Welcome back to Valorant " + userName);
}

// displayWelcomeMessageForUser(user_name);