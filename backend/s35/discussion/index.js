// Greater than operator
db.users.find({
	age: {
		$gte: 82
	}
});

//  Less than operator

db.users.find({
	age: {
		$lt: 80
	}
});


// Regex Operator
db.users.find({
	firstName: {
		$regex: 's',
		$options: 'i'
	}
});

db.users.find({
	lastName: {
		$regex: 'T',
		$options: 'i'
	}
});


// Combining Operators
db.users.find({
	age: {
		$gt: 70
	},
	lastName: {
		$regex: 'g'
	}
});

db.users.find({
	age: {
		$lte: 72
	},
	firstName: {
		$regex: 'j',
		$options: 'i'
	}
});

// Field Projection
db.users.find({}, {
	"_id": 0
});

db.users.find({}, {
	"firstName": 1
});
// bakit na labas si ID 
// medj special si ID

db.users.find({}, {
	"_id": 0,
	"firstName": 1
});



// data
db.users.insertMany([
 			{
                firstName: "Jane",
                lastName: "Doe",
                age: 21,
                contact: {
                    phone: "87654321",
                    email: "janedoe@gmail.com"
                },
                courses: [ "CSS", "Javascript", "Python" ],
                department: "none"
						},
            {
                firstName: "Stephen",
                lastName: "Hawking",
                age: 76,
                contact: {
                    phone: "87654321",
                    email: "stephenhawking@gmail.com"
                },
                courses: [ "Python", "React", "PHP" ],
                department: "none"
            },
            {
                firstName: "Neil",
                lastName: "Armstrong",
                age: 82,
                contact: {
                    phone: "87654321",
                    email: "neilarmstrong@gmail.com"
                },
                courses: [ "React", "Laravel", "Sass" ],
                department: "none"
            }
]);