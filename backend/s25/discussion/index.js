// console.log("love, objects");

// [SECTION] Objects
/*
 -  an object is a data type that is used to represent real world objects
 - create properties and methods/functionalities
*/

// creating objects using intitiallizers/objects literals "{}"
let cellphone = {
	//  key:value pair
	name: "Nokia 3210",
	manufacturerDate: 1999
};

console.log("Result from creating objects using initializers/object literals");
console.log(cellphone);
console.log(typeof cellphone);


// creating objects using a constructor function
// the function acts as a blueprint
function Laptop(name, manufactureDate) {
	// "this" - is used to assign a value to the arguement. this.key:value;
	this.name = name;
	this.manufactureDate = manufactureDate;
}

// multiple instance of an object using the "new" keyword
// this method is called instantiation
let laptop = new Laptop('Lenovo', 2008);
console.log("Result from creating objects using constructor function");
console.log(laptop);

let laptop2 = new Laptop('Macbook Air', 2020);
console.log("Result from creating objects using constructor function");
console.log(laptop2);


// [SECTION] Accessing Object Properties

// using square bracket notation
console.log("Result from square bracket notation " + laptop2["name"]);

// using dot notation.
console.log("Result from square dot notation " + laptop2.name);

// Use dot notation whenever we are accessing objects and square bracket for arrays.

// access array objects
let array = [laptop, laptop2];

console.log(array[0]["name"]);
console.log(array[0].name);


// [SECTION] Adding/Deleting/Reassigning Object Properties

// empty object
let car = {};
// empty object using constructor function/instantiation
// pre-defined function
let myCar = new Object();

//adding object properties using dot notation
car.name = "Honda Civic";
console.log("Result from adding properties using dot notation");
console.log(car);

// adding object properties using square bracket notation
car["manufacturing date"] = 2019;
console.log("Result from adding properties using square bracket notation");
console.log(car);
console.log(car["manufacturing date"]);
// we cannot access the object property using dot notation if the key has spaces
// console.log(car.manufacturing date);


// deleting object properties
delete car["manufacturing date"];
console.log("Result from deleting properties");
console.log(car);


// reassigning object properties
car.name = "Honda Civic Type R";
console.log("Result from reassigning properties");
console.log(car);


// [SECTION] Objects Methods
/*
	- a method is a function which acts as a property of an object
*/

let person = {
	name: 'Barbie',
	greet: function(){
		// console.log('Hello! My name is ' + person.name);
		console.log('Hello! My name is ' + this.name);
	}
}

console.log(person);
console.log("Result form object methods: ");
// greet() is now called a method
// hindi na function yugn function sa loob ng greet kasi 
// no need for console sa labas kasi may console.log na sa lob ng greet
person.greet();


//  adding methods to objects
person.walk = function(){
	console.log(this.name + " walked 25 steps forward");
}

person.walk();

let friend = {
	// values can be string
	name: "Ken",
	address: {
		city: "Austin",
		state: "Texas",
		country: "USA"
	},
	email: ['ken@gmail.com', 'ken@mail.com'],
	// Nested- an object within an object
	introduce: function(person) {
		console.log('Nice to meet you ' + person.name + " I am " + this.name + ' from ' + this.address.city + ' ' + this.address.state);
	}
}

friend.introduce(person);