db.fruits.aggregate([
	// $match - used to match or get documents that is satisfies the condition
	// $match is similar to find().
	// You can use query operators to make your criteria more flexible
	/*
		$match -> Apple, Kiwi, Banana
	*/
	{ $match: { onSale: true}},
	// $group - allows us to group together documents and create an analysis out of the group elements
	// _id: $supplier_id
	/*
		Apple = 1.0
		Kiwi = 1.0
		Banana = 2.0

		grouping based on the supplier_id, ilang Id meron tayo?
		_id: 1.0
			Apple and Kiwi

			total: sum of the fruit stock of 1.0
			total: Apple stocks + Kiwi Stocks
			total: 20 + 25
			total: 45

		_id: 2
			Banana

			total: sum of the fruit stock of 2.0
			total: Banana stocks
			total: 15
	*/
	// '$' - command / 
	{ $group: { _id: "$supplier_id", total: { $sum: "$stock"}}}
]);


// Result from Mongosh
{
  _id: 1,
  total: 45
}
{
  _id: 2,
  total: 15
}


// $avg - gets the average of the values of the given field per group
db.fruits.aggregate([
	{ $match: { onSale: true}},
	{ $group: { _id: '$supplier_id', avgStocks: { $avg: '$stock'}}},
	// $project - can be used when we are aggregating the data to include or exclude fields form the returned results
	{ $project: { _id: 0 }}
]);




db.fruits.aggregate([
	{ $match: { onSale: true}},
	{ $group: { _id: '$supplier_id', maxPrice: { $max: '$price'}}},
	// $sort - ro change the order of the aggregated result
	// Providing a value of -1 will sort the aggregated results in a reverse order
	{ $sort: { maxPrice: -1 }}
]);



// $unwind - deconstructs the array field from a collection with an array value to output result for each element
db.fruits.aggregate([
	{ $unwind: "$origin"}
]);



/*db.fruits.aggregate([
	{ $match: { onSale: true}},
	{ $group: { _id: '$supplier_id', maxPrice: { $max: '$price'}}},
	{ $sort: { maxPrice: 1 }}
]);*/


db.fruits.aggregate([
	{ $unwind: '$origin'},
	{ $group: { _id: '$origin', kinds: { $sum: 1}}}
]);