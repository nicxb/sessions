const User = require('../models/User.js');
const bcrypt = require('bcrypt'); // initialize bcryt
const auth = require('../auth.js');

const Course = require('../models/Course.js');

module.exports.checkEmailExists = (request_body) => {
	return User.find({email: request_body.email}).then((result, error) => {
		if(error) {
			return {
				message: error.message 
			};
		}

		if (result.length <= 0){
			return false;
		}

		// This will only return true if there are no error AND there is a result from the query.
		return true;
	})
}

module.exports.registerUser = (request_body) => {
	let new_user = new User({
		firstName: request_body.firstName,
		lastName: request_body.lastName,
		email: request_body.email,
		mobileNo: request_body.mobileNo,
		password: bcrypt.hashSync(request_body.password, 10) 
		// 10 - the number of hashes
		// the more truns, the more secure but the more lag in performance.
	});

	return new_user.save().then((registered_user, error) => {
		if(error){
			return {
				message: error.message 
			};
		}

		return {
			message: 'Successfully registerd a user!',
			data: registered_user
		};
	}).catch(error => console.log(error));
}

module.exports.loginUser = (request, response) => {
	return User.findOne({email: request.body.email}).then(result => {
		// Checks if a user is found with an existing email
		if(result == null) {
			return response.send({
				message: "The user isn't registered yet."
			})
		}

		// if a user was found with an existing email, then check if the password of that user matched the input from the request body
		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if(isPasswordCorrect){
			return response.send({accessToken: auth.createAccessToken(result)});
		} else {
			return response.send({
				message: 'Your password is incorrect.'
			})
		}
	}).catch(error => response.send(error));
}

module.exports.getProfile = (request_body) => {
	return User.findOne({_id: request_body.id}).then((user, error) => {
		if(error){
			return {
				message: error.message 
			}
		}
		
		// result.password = '';
		// return result;		
		user.password = '';
		return user;


	}).catch(error => console.log(error));
}

module.exports.enroll = async (request, response) => {
	// Blocks this function if the user is an admin
	if(request.user.isAdmin){
		return response.send('Action Forbidden');
	}

	// [SECTION] Updating user collection
	// This variable will return true once the user data has been updated
	let isUserUpdated = await User.findById(request.user.id).then(user => {
		let new_enrollment = {
			courseId: request.body.courseId
		}

		user.enrollments.push(new_enrollment);

		return user.save().then(updated_user => true).catch(error => error.message);
	})

	if(isUserUpdated !== true){
		return response.send({ message: isUserUpdated});
	}

	// [SECTION] Updating course collection
	let isCourseUpdated = await Course.findById(request.body.courseId).then(course => {
		let new_enrollee = {
			userId: request.user.id 
		}

		course.enrollees.push(new_enrollee);

		return course.save().then(update_course => true).catch(error => error.message)
	})

	if(isCourseUpdated !== true){
		return response.send({ message: isCourseUpdated});
	}

	// [SECTION] Once isUserUpdated and isCourseUpdated return true
	if(isUserUpdated && isCourseUpdated){
		return response.send({ message: 'Enrolled successfully!' });
	}
}

module.exports.getEnrollments = (request, response) => {
	User.findById(request.user.id)
	.then(result => {response.send(result.enrollments)})
	.catch(error => response.send(error.message));
}

